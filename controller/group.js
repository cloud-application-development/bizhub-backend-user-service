const {verifyAndDecodeToken, verifyClusterAccess} = require('../service/authenticator');
const Firestore = require('@google-cloud/firestore');
const {v4: uuid} = require('uuid');
const rootCollectionName = 'tenants';
const usersCollection = 'users';
const groupsCollection = 'groups';
const serviceAccount = require('../service_account_keyfile.json');

const db = new Firestore({
    projectId: serviceAccount.project_id,
    keyFilename: './service_account_keyfile.json',
});

const createNewGroup = async (req, res) => {

  const token = await verifyAndDecodeToken(req.header('token'));
  const tenantId = token.firebase.tenant;

  const clusterAccess = verifyClusterAccess(tenantId);

  if (!token || !clusterAccess) { //if token could not be verified
      res.status(403).send('Forbidden')
      return
  }

  const isAdmin = await userIsTenantAdmin(token.firebase.tenant, token.uid);

  if (!isAdmin) {
      console.log('Access denied for user ' + token.uid + ', of tenant ' + token.firebase.tenant + '. User needs to be admin')
      res.status(403).send('Forbidden')
      return
  }

  let groupDocumentData = {
    groupId: uuid(),
    name: req.body.name,
    type: req.body.type,
  }

  db.collection(rootCollectionName).doc(token.firebase.tenant).collection(groupsCollection).doc(groupDocumentData.groupId).set(groupDocumentData).then(() => {
    res.status(201).send('Created')
  })
}

const getAllGroups = async (req, res) => {

  const token = await verifyAndDecodeToken(req.header('token'));
  const tenantId = token.firebase.tenant;

  const clusterAccess = verifyClusterAccess(tenantId);

  if (!token || !clusterAccess) { //if token could not be verified
      res.status(403).send('Forbidden')
      return
  }

  const groups = []

  await db.collection(rootCollectionName).doc(token.firebase.tenant).collection(groupsCollection).get().then((querySnapshot) => {
      querySnapshot.forEach(snapshot => {
          let data = snapshot.data();

          groups.push(data)
      })
      res.json(groups);
  })
  res.end()
}

const addUserToGroup = async (req, res) => {

  const token = await verifyAndDecodeToken(req.header('token'));
  const tenantId = token.firebase.tenant;

  const clusterAccess = verifyClusterAccess(tenantId);

  if (!token || !clusterAccess) { //if token could not be verified
      res.status(403).write('Forbidden')
      return
  }

  const {groupId} = req.body
  const {userObject} = req.body


  //TODO add the user to a user collection in the group
  db.collection(rootCollectionName).doc(token.firebase.tenant).collection(groupsCollection).doc(groupId).collection(usersCollection).doc(userObject.userId).set(userObject).then(() => {
    res.status(201).write('Created')
  })

  //TODO add the group to the user (collection?)
  db.collection(rootCollectionName).doc(token.firebase.tenant).collection(usersCollection).doc(userObject.userId).update({groups: Firestore.FieldValue.arrayUnion(groupId)}).then(() => {
    res.status(201).write('Created')
  })

  res.end()
}

const removeUserFromGroup = async (req, res) => {

  const token = await verifyAndDecodeToken(req.header('token'));
  const tenantId = token.firebase.tenant;

  const clusterAccess = verifyClusterAccess(tenantId);

  const {groupId} = req.params
  const {userId} = req.params

  if (!token || !clusterAccess) { //if token could not be verified
    res.status(403).write('Forbidden')
    return
  }

  const isAdmin = await userIsTenantAdmin(token.firebase.tenant, token.uid);

  if (!isAdmin || userId !== token.uid ) {
    console.log('Access denied for user ' + token.uid + ', of tenant ' + token.firebase.tenant + '. User needs to be admin or himself')
    res.status(403).send('Forbidden')
      return
  }

  //TODO add the user to a user collection in the group
  db.collection(rootCollectionName).doc(token.firebase.tenant).collection(groupsCollection).doc(groupId).collection(usersCollection).doc(userId).delete().then(() => {
    res.status(201).write('Created')
  })

  //TODO add the group to the user (collection?)
  db.collection(rootCollectionName).doc(token.firebase.tenant).collection(usersCollection).doc(userId).update({groups: Firestore.FieldValue.arrayRemove(groupId)}).then(() => {
    res.status(201).write('Created')
  })

  res.end()
}

async function userIsTenantAdmin(tenant, userId) {
  const user = await getUserFromFirestore(tenant, userId);
  if (user) { //if user exists
      return (user.admin || user.admin === 'true') //make sure booleans and strings are taken into account
  }
  return false;
}

async function getUserFromFirestore(tenant, userId) {
  let userData = ''
  await db.collection(rootCollectionName).doc(tenant).collection(usersCollection).doc(userId).get().then((querySnapshot) => {
      if (!querySnapshot.empty) {
          userData = querySnapshot.data()
      } else {
          return false; //user not found
      }
  })
  return userData;
}

module.exports = {
  createNewGroup,
  getAllGroups,
  addUserToGroup,
  removeUserFromGroup
}