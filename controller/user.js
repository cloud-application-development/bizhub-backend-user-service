const { verifyAndDecodeToken, updateUserForTenant, createUserForTenant, deleteUserFromTenant, verifyClusterAccess } = require('../service/authenticator');
const { uploadUserImageToBucket, deleteUserImageFromBucket } = require('../service/upload')
const Firestore = require('@google-cloud/firestore');
const { v4: uuid } = require('uuid');
const ROOT_COLLECTION = 'tenants';
const USER_COLLECTION = 'users';
const postsCollection = 'posts';
const serviceAccount = require('../service_account_keyfile.json');

const db = new Firestore({
    projectId: serviceAccount.project_id,
    keyFilename: './service_account_keyfile.json',
});


const newUser = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }


    const isAdmin = await userIsTenantAdmin(token.firebase.tenant, token.uid);

    if (!isAdmin) {
        console.log('Access denied for user ' + token.uid + ', of tenant ' + token.firebase.tenant + '. User needs to be admin')
        res.status(403).send('Forbidden')
        return
    }

    const user = req.body

    //if displayName is undefined, user name and surname as displayName
    const displayName = user.displayName ? user.displayName : (user.name + ' ' + user.surname)
    const name = user.name
    const surname = user.surname
    const email = user.email
    const password = user.password
    const admin = user.admin //make sure admin is a boolean
    const createdAt = Date.now()

    let identityData = {
        email: email,
        password: password,
        name: name,
        surname: surname,
    }

    //Send identityData to method createUserForTenant to create an user in the identity platform
    const identityUser = await createUserForTenant(tenantId, identityData);

   if (!identityUser) {
        console.log("User not created in Identity Platform.")
        return
    }

    let userDataForFirestore = {
        userId: identityUser.uid,
        displayName: displayName,
        tenantId: tenantId,
        name: name,
        surname: surname,
        email: email,
        admin: admin,
        createdAt: createdAt,
        photoURL: '',
    }


    await uploadUserImageToBucket('', tenantId, userDataForFirestore.userId)
    .then((imageURL) => {
        userDataForFirestore.photoURL = imageURL
    }).catch((err) => {
        res.status(400).write('Bad Request, ' + err)
    })
    
    db.collection(ROOT_COLLECTION).doc(tenantId).collection(USER_COLLECTION).doc(identityUser.uid).set(userDataForFirestore).then(() => {
        res.status(201).send('Created')
    })
};

const updateUser = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const userIdToUpdate = req.params.userId
    const isAdmin = await userIsTenantAdmin(tenantId, token.uid);

    if (!isAdmin || token.uid !== userIdToUpdate) {
        console.log('Access denied for user ' + token.uid + ', of tenant ' + tenantId + '. User needs to be admin or himself')
        res.status(403).send('Forbidden')
        return
    }

    //Clean up the request payload
    let sanitizedRequestPayload = sanatizeUserRequestPayload(req)


    //If rawimage vorhanden
    if(sanitizedRequestPayload.rawImage != undefined) {
        await uploadUserImageToBucket(sanitizedRequestPayload.rawImage, tenantId, userIdToUpdate)
        .then((imageURL) => {
            sanitizedRequestPayload.photoURL = imageURL
            delete sanitizedRequestPayload.rawImage
        }).catch((err) => {
            res.status(400).write('Bad Request, ' + err)
        })
    }

    //Prepare request payload for identity
    const identityUserInformation = JSON.parse(JSON.stringify({
        email: sanitizedRequestPayload.email,
        password: sanitizedRequestPayload.password,
        displayName: sanitizedRequestPayload.displayName,
        photoURL: sanitizedRequestPayload.photoURL
    }));

    //remove password from payload
    delete sanitizedRequestPayload.password

    await updateUserForTenant(tenantId, userIdToUpdate, identityUserInformation).then((result) => {
        db.collection(ROOT_COLLECTION).doc(tenantId).collection(USER_COLLECTION).doc(userIdToUpdate).set(
            sanitizedRequestPayload,
            { merge: true }
        ).then(() => {
            updateUserInformationInPosts(sanitizedRequestPayload,userIdToUpdate, tenantId)
            updateUserInformationInComments(sanitizedRequestPayload,userIdToUpdate, tenantId)
            res.status(200).write('Created') });
    }).catch((error) => {
        res.status(500).write(error)
    })

    res.end();
}


const getUsers = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const isAdmin = await userIsTenantAdmin(tenantId, token.uid);

    if (!isAdmin) {
        console.log('Access denied for user ' + token.uid + ', of tenant ' + tenantId + '. User needs to be admin!')
        res.status(403).write('Forbidden')
        return;
    }

    const users = []

    await db.collection(ROOT_COLLECTION).doc(tenantId).collection(USER_COLLECTION).get().then((querySnapshot) => {
        querySnapshot.forEach(snapshot => {
            let data = snapshot.data();

            users.push(data)
        })
        res.json(users);
    })

    res.end()
}

const getUserById = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const userId = req.params.userId

    db.collection(ROOT_COLLECTION).doc(tenantId).collection(USER_COLLECTION).doc(userId).get().then((snapshot) => {
        let data = snapshot.data();
        res.json(data);
    })

}


const deleteUser = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const isAdmin = await userIsTenantAdmin(tenantId, token.uid);
    const userIdDelete = req.params.userId

    if (!isAdmin) {
        console.log('Access denied for user ' + token.uid + ', of tenant ' + tenantId + '. User needs to be admin!')
        res.status(403).send('Forbidden')
        return;
    }
    const deletedUser = deleteUserFromTenant(tenantId, userIdDelete);
    if (!deletedUser) {
        console.log("User not deleted from Identity Platform.")
        return
    }

    db.collection(ROOT_COLLECTION).doc(tenantId).collection(USER_COLLECTION).doc(userIdDelete).delete();
    console.log("deleting: " + userIdDelete)
    res.status(200).send('Success');
}

const updateUserInformationInPosts = async (userInformation, userIdToUpdate, tenant) => {
    const newUserInformation = userInformation
    delete newUserInformation.admin

   const ref = db.collection(ROOT_COLLECTION).doc(tenant).collection(postsCollection).where('author.userId', '==', userIdToUpdate).get().then((querySnapshot) => {
       querySnapshot.forEach((doc) => {
           db.collection(ROOT_COLLECTION).doc(tenant).collection(postsCollection).doc(doc.data().postId).set({author: newUserInformation}, {merge:true})
       })
   })
}

const updateUserInformationInComments = async (userInformation, userIdToUpdate, tenant) => {
    const newUserInformation = JSON.parse(JSON.stringify({
        displayName: userInformation.displayName,
        photoURL: userInformation.photoURL
    }))

   const postCollectionRef = db.collection(ROOT_COLLECTION).doc(tenant).collection(postsCollection)

   postCollectionRef.get().then((querySnapshot) => {
       querySnapshot.forEach((postDocument) => {
        const commentCollectionRef = postCollectionRef.doc(postDocument.data().postId).collection('comments')
        
        commentCollectionRef.where('authorId', '==', userIdToUpdate).get().then((querySnapshot) => {
            querySnapshot.forEach((commentDocument) => {
                console.log(commentDocument.data())
                commentCollectionRef.doc(commentDocument.data().commentId).set(newUserInformation, {merge:true})
            })
        })
       })
   })
}

const sanatizeUserRequestPayload = (req) => {
    return JSON.parse(JSON.stringify({
        name: (req.body.name === null || req.body.name === undefined) ? undefined : req.body.name,
        surname: (req.body.surname === null || req.body.surname === undefined) ? undefined : req.body.surname,
        email: (req.body.email === null || req.body.email === undefined) ? undefined : req.body.email,
        admin: (req.body.admin === null || req.body.admin === undefined) ? undefined : req.body.admin,
        displayName: (req.body.displayName === null || req.body.displayName === undefined) ? undefined : req.body.displayName,
        photoURL: (req.body.photoURL === null || req.body.photoURL === undefined) ? undefined : req.body.photoURL,
        rawImage: (req.body.rawImage === null || req.body.rawImage === undefined) ? undefined : req.body.rawImage,
        password: (req.body.password === null || req.body.password === undefined) ? undefined : req.body.password
    }));
}

async function userIsTenantAdmin(tenant, userId) {
    const user = await getUserFromFirestore(tenant, userId);
    if (user) { //if user exists
        return (user.admin || user.admin === 'true') //make sure booleans and strings are taken into account
    }
    return false;
}


async function getUserFromFirestore(tenant, userId) {
    let userData = ''
    await db.collection(ROOT_COLLECTION).doc(tenant).collection(USER_COLLECTION).doc(userId).get().then((querySnapshot) => {
        if (!querySnapshot.empty) {
            userData = querySnapshot.data()
        } else {
            return false; //user not found
        }
    })
    return userData;
}


module.exports = {
    newUser,
    getUsers,
    deleteUser,
    getUserById,
    updateUser
}