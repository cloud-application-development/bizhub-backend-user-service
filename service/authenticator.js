var admin = require('firebase-admin');
var {getAuth} = require('firebase-admin/auth');
var serviceAccount = require('../service_account_keyfile.json');

var app = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

const verifyAndDecodeToken = async (idToken) => getAuth(app).verifyIdToken(idToken)
    .then((decodeToken) => {
        return decodeToken
    }).catch((error) => {
            console.log("Authentication failed!" + error.message)
            return false
        }
    );

const createTenant = async (tenantName) => {
    return new Promise((resolve, reject) => {
        getAuth(app).tenantManager().createTenant({
            displayName: tenantName,
            emailSignInConfig: {
                enabled: true,
                passwordRequired: true, // Email link sign-in enabled.
            }
        }).then((createdTenant) => {
            resolve(createdTenant.toJSON());
        }).catch((error) => {
            console.log("tenant could not be created. " + error.message);
            reject(false)
        });
    })
}

const createUserForTenant = async (tenantId, userObject) => {
    return new Promise((resolve, reject) => {
        getAuth(app).tenantManager().authForTenant(tenantId).createUser({
            email: userObject.email,
            emailVerified: false,
            password: userObject.password,
            displayName: userObject.name + ' ' + userObject.surname,
            disabled: false
        }).then((userRecord) => {
            resolve(userRecord);
        }).catch((error) => {
            console.log('Error creating new user:', error);
            reject(false);
        });
    })
}

const updateUserForTenant = (tenantId, userId, userObject) => {
    return new Promise((resolve, reject) => {
        getAuth(app).tenantManager().authForTenant(tenantId).updateUser(userId, userObject).then((updateRecord) => {
            resolve(updateRecord)
        }).catch((error) => {
            console.log('Error updating user:', error);
            reject(new Error('Failed to update identity platform user'));
        })
    })
}

const deleteUserFromTenant = async (tenantId, userId) => {
    return new Promise((resolve, reject) => {
        getAuth(app).tenantManager().authForTenant(tenantId).deleteUser(userId).then((deleteRecord) => 
        { console.log('Successful deleted user from identity platform'); resolve(deleteRecord); }).catch((error) => { console.log('Error deleting user from identity platfom: ', error); reject(false); });
    })
}

const verifyClusterAccess = (tenantId) => {
    let clusterOwner = process.env.CLUSTER_OWNER;
    return clusterOwner === 'bizhub' ? true: tenantId === clusterOwner;
}

module.exports = {
    verifyAndDecodeToken,
    createTenant,
    createUserForTenant,
    updateUserForTenant,
    deleteUserFromTenant,
    verifyClusterAccess
}