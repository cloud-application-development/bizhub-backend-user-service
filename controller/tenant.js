const {verifyAndDecodeToken, createTenant, createUserForTenant, verifyClusterAccess} = require('../service/authenticator');
const Firestore = require('@google-cloud/firestore');
const {v4: uuid} = require('uuid');
const ROOT_COLLECTION = 'tenants';
const USER_COLLECTION = 'users';
const serviceAccount = require('../service_account_keyfile.json');
const {uploadUserImageToBucket} = require("../service/upload");

const db = new Firestore({
    projectId: serviceAccount.project_id,
    keyFilename: './service_account_keyfile.json',
});


const newTenant = async (req, res) => {


    //TODO verify request (mail is valiid/payment method, etc.)
    //TODO check and inform user about tenant name constraints (display_name should start with a letter and only consist of letters, digits and hyphens with 4-20 characters.)
    const tenantData = req.body

    const tenantName = tenantData.tenantName
    const userObject = tenantData.userObject
    const tier = tenantData.tier.toLowerCase()

    const allowedTiers = ['free','premium','enterprise']
    if (!allowedTiers.includes(tier)) {
        res.status(400).send('Bad Request')
        return;
    }

    const createdTenant = await createTenant(tenantName)
    if (!createdTenant) {
        res.status(500).send('Internal Server Error')
        return;
    }

    const createdUser = await createUserForTenant(createdTenant.tenantId, userObject)
    if (!createdUser) {
        res.status(500).send('Internal Server Error')
        return;
    }

    let userDataForFirestore = {
        userId: createdUser.uid,
        displayName: userObject.name + ' ' + userObject.surname,
        tenantId: createdTenant.tenantId,
        name: userObject.name,
        surname: userObject.surname,
        email: userObject.email,
        admin: true,
        createdAt: Date.now(),
    }


    await uploadUserImageToBucket('', createdTenant.tenantId, userDataForFirestore.userId)
    .then((imageURL) => {
        userDataForFirestore.photoURL = imageURL
    }).catch((err) => {
        res.status(400).write('Bad Request, ' + err)
    })

    db.collection(ROOT_COLLECTION).doc(createdTenant.tenantId).set({
        tier: tier,
    }).then(() => {
    }).catch((error) => {
        res.status(500).send('Internal Server Error')
        console.log("failed to write tenant tier: " + error.message)
    });


    db.collection(ROOT_COLLECTION).doc(createdTenant.tenantId).collection(USER_COLLECTION).doc(createdUser.uid).set(userDataForFirestore).then(() => {
        res.status(201).json({tenantId: createdTenant.tenantId})
    }).catch((error) => {
        res.status(500).send('Internal Server Error')
        console.log("firebase entry failed: " + error.message)
    });

}

const getTenant = async (req, res) => {
    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const tenantIdFromRequest = req.params.tenantId
    const tenantIdFromToken = tenantId

    if (tenantIdFromRequest !== tenantIdFromToken) { //if trying to get another tenants data
        res.status(403).send('Forbidden')
        return;
    }

    db.collection(ROOT_COLLECTION).doc(tenantIdFromToken).get().then((snapshot) => {
        let data = snapshot.data();
        res.json(data);
    })

}


const getTenantByName = async (req, res) => {

    const tenantName = req.params.tenantName
    if (tenantName.length < 4) {
        res.status(400).send('Bad Request')
        return
    }

    await db.collection(ROOT_COLLECTION).get().then((snapshot) => {
        let tenantIds = []
        snapshot.forEach(tenant => {
            if(tenant.id.includes(tenantName)){
                tenantIds.push(tenant.id)
            }
        })
        res.json(tenantIds);
    })
}


module.exports = {
    newTenant,
    getTenant,
    getTenantByName,
}