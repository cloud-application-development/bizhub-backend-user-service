const express = require('express')
const userController = require('../controller/user')
const tenantController = require('../controller/tenant')
const groupController = require('../controller/group')

var router = express.Router()

router.post('/users', userController.newUser);
router.put('/users/:userId', userController.updateUser)
router.get('/users', userController.getUsers);
router.get('/users/:userId', userController.getUserById);
router.delete('/users/:userId', userController.deleteUser);


router.post('/tenants', tenantController.newTenant);
router.get('/tenants/:tenantId', tenantController.getTenant);
router.get('/tenant-id/:tenantName', tenantController.getTenantByName);

router.post('/groups', groupController.createNewGroup)
router.get('/groups', groupController.getAllGroups)
router.post('/groups/:groupId', groupController.addUserToGroup)
router.delete('/groups/:groupId/users/:userId', groupController.removeUserFromGroup)


module.exports = router

